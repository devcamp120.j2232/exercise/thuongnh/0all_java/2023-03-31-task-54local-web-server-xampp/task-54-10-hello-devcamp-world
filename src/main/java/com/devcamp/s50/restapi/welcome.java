package com.devcamp.s50.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/api")
public class welcome {
    @CrossOrigin
    @GetMapping("/devcamp-welcome")
    public String nice() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now= new Date();
        return String.format("chúc ngủ ngon baby. It is %s!.", dateFormat.format(now));
    }


    
}
